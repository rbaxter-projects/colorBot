const Jimp = require('jimp');
const kdTree = require('k-d-tree');
const _ = require('lodash');

class ImageService {
  constructor(fileName) {
    let _this = this;
    this.fileName = fileName;
    this.kdTree = new kdTree([], _this.distance);
    this.seenColors = new Set();
    this.foundColors = {};

    //FOR FREQUENCY
    this.oldColorFreq = {};
    this.newColorFreq = {};

    this.colorMap = {};
  }

  distance (a, b) {
    return Math.pow((b.coordinates[0] - a.coordinates[0])*0.35, 2)
      + Math.pow((b.coordinates[1] - a.coordinates[1])*.45, 2)
      + Math.pow((b.coordinates[2] - a.coordinates[2])*.20, 2)
      + Math.pow((b.coordinates[3] - a.coordinates[3]), 2);
  }

  buildTree(imagePath) {
    return new Promise((resolve, reject) => {
      let _this = this;
      let percentage = 0;
      Jimp.read(`./images/input_${imagePath}.jpg`, function (err, image) {

        if (err) {
          reject(err);
          return;
        }

        //~~~~~~~~~~~~~~~~~~~~TREE IMPLEMENTATION~~~~~~~~~~~~~~~~~
        image.scan(0, 0, image.bitmap.width, image.bitmap.height, function (x, y, idx) {
          var red   = this.bitmap.data[ idx + 0 ];
          var green = this.bitmap.data[ idx + 1 ];
          var blue  = this.bitmap.data[ idx + 2 ];
          var alpha = this.bitmap.data[ idx + 3 ];
          let node = {
            coordinates: [red, green, blue, alpha],
          };
          let color = Jimp.rgbaToInt(red, green, blue, alpha);
          if (!_this.seenColors.has(color)) {
            _this.seenColors.add(color);
            _this.kdTree.insert(node);
          }
        });
        resolve();
      });
    });
  }

  buildFrequency(fileName) {
    return new Promise((resolve, reject) => {
      let _this = this;
      Jimp.read(`./images/input_${fileName}.jpg`, function (err, image) {

        if (err) {
          reject(err);
          return;
        }

        _this._loopThroughPixels(image, (image, x, y) => {
          let color = image.getPixelColor(x, y);
          if (_this.oldColorFreq[color]) {
            _this.oldColorFreq[color]++;
          } else {
            _this.oldColorFreq[color] = 1;
          }
        });

        resolve();
      });
    });
  }

  _loopThroughPixels(image, cb) {
    let width = image.bitmap.width;
    let height = image.bitmap.height;
    let x = 0;
    let y = 0;

    while(x < width) {
      y = 0;
      while(y < height) {
        cb(image, x, y);
        y++;
      }
      x++;
    }
  }

  processImage(fileName, oldFile) {
    // return this.buildTree(oldFile).then(() => {
    //   return this.convertImage(fileName);
    // });
    if ((Math.floor(Math.random() * 2) === 0)) {
       return this.buildFrequency(oldFile).then(() => {
        return this.convertImage(fileName);
       });
    } else {
      return this.buildTree(oldFile).then(() => {
        return this.convertImageTree(fileName);
      });
    }
  }

  // // FOR K-D-TREE
  convertImageTree(imagePath) {
    return new Promise((resolve, reject) => {
      let _this = this;
      Jimp.read(`./images/input_${imagePath}.jpg`, (err, image) => {
        if (err) {
          console.log('error in convertImage read' + err);
        }
        let imageCopy = image.clone();
        let searchesPrevented = 0;
        //~~~~~~~~~~~~~TREE IMPLEMENTATION~~~~~~~~~~~~
        imageCopy.scan(0, 0, imageCopy.bitmap.width, imageCopy.bitmap.height, function (x, y, idx) {
          let color = imageCopy.getPixelColor(x,y);
          let closestColor;
          if (_this.foundColors[color] !== undefined) {
            searchesPrevented++;
            closestColor = _this.foundColors[color];
          } else {
            closestColor = _this._getClosestColor(color);
            _this.foundColors[color] = closestColor;
          }

          imageCopy.setPixelColor(closestColor, x, y);
        });
        let filePath = `./images/output_${_this.fileName}.png`;
        imageCopy.write(filePath, () => {
          resolve(filePath);
        });
      });
    });
  }

  convertImage(imagePath) {
    return new Promise((resolve, reject) => {
      let _this = this;
      Jimp.read(`./images/input_${imagePath}.jpg`, (err, image) => {
        if (err) {
          console.log('error in convertImage read' + err);
        }
        let imageCopy = image.clone();

        _this._loopThroughPixels(imageCopy, (image, x, y) => {
          let color = image.getPixelColor(x, y);
          if (_this.newColorFreq[color]) {
            _this.newColorFreq[color]++;
          } else {
            _this.newColorFreq[color] = 1;
          }
        });

        _this._buildColorMap(_this);

        _this._loopThroughPixels(imageCopy, (image, x, y) => {
          let color = image.getPixelColor(x, y);
          image.setPixelColor(_this.colorMap[color], x, y);
        })

        let filePath = `./images/output_${_this.fileName}.png`;
        imageCopy.write(filePath, () => {
          resolve(filePath);
        });
      });
    });
  }

  _buildColorMap(_this) {
    let availColorKeys = _.sortBy(Object.keys(_this.oldColorFreq), (key) => {
      return -_this.oldColorFreq[key];
    });

    let inputKeys = _.sortBy(Object.keys(_this.newColorFreq), (key) => {
      return -_this.newColorFreq[key];
    });

    _this.colorMap = {};
    _.each(inputKeys, (key, idx) => {
      let index = idx > availColorKeys.length ? availColorKeys.length - 1 : idx;
      _this.colorMap[key] = parseInt(availColorKeys[index]);
    });
  }

  _getClosestColor(color) {
    let rgbColor = Jimp.intToRGBA(color);
    let errorColor = Jimp.rgbaToInt(255, 0, 255, 0);
    let nearest = this.kdTree.nearest({coordinates: [rgbColor.r, rgbColor.g, rgbColor.b, rgbColor.a]}, 1)[0];
    return nearest.length ? Jimp.rgbaToInt(nearest[0].coordinates[0], nearest[0].coordinates[1], nearest[0].coordinates[2], nearest[0].coordinates[3]) : errorColor;
  }
}

module.exports = ImageService;