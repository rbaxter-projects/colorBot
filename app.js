

const _ = require('lodash');
const ImageService = require('./image.service.js');
const Twit = require('twit');
const fs = require('fs');
const request = require('request');
let fileCounter = 1;

let T = new Twit({
  consumer_key: 'wNZZmGLm3TqOUZ0JTCE78IsAt',
  consumer_secret: '3bkWq7E653CZOM8mdkOffKx9AcXr5LfmZ8B8CQ9e7rhQ4WTGlC',
  access_token: '862783990167728129-ibWb58t2bC7PKmQW1E2Qzkj09ofhRT4',
  access_token_secret: 'FCnrlLNnkIxLuXlkXT8m0cXTK3ikxNn35D7wgUT6CMm1h',
  timeout_ms: 60 * 1000,
});

let stream = T.stream('statuses/filter', { track: '@img_color_swap' });

stream.on('tweet', function (tweet) {
  processTweet(tweet).then((fileName) => {
    console.log(`Tweeting back.  filename:  ${fileName}`);
    tweetBack(fileName, tweet.user.screen_name);
  });
});

stream.on('error', (err) => {
  console.log('weird');
});

function tweetBack(fileName, userName) {
  console.log('TEST'+fileName);
  let b64content = fs.readFileSync(fileName, { encoding: 'base64' });

  //function in param is the call back
  uploadMedia(b64content, function (err, data, response) {
    if (err) {
      console.log('handle errors');
    }
    var mediaIdStr = data.media_id_string;
    var meta_params = { media_id: mediaIdStr };
    T.post('media/metadata/create', meta_params, function (err, data, response) {
      if (!err) {
        var params = { status: `Enjoy your image @${userName}!`, media_ids: [mediaIdStr] }
        T.post('statuses/update', params, function (err, data, response) {
          console.log('Another good job!');
        });
      }
    });
  });
}

function uploadMedia(content, callBack) {
  T.post('media/upload', { name: 'Fun Image', media_data: content}, callBack);
}


function processTweet(tweet) {
  return new Promise((resolve, reject) => {
    downloadImages(tweet.extended_entities.media, function (fileName, oldFile) {
      let is = new ImageService(fileName);
      is.processImage(fileName, oldFile).then((fileName) => {
        resolve(fileName, tweet.user.screen_name);
      });
    });
  });
}

function downloadImages(images, cb) {
  if (images.length) {
    let url = images[0].media_url;
    request.head(url, function (err, res, body) {
      let oldFile = fileCounter - 1;
      let fileName = fileCounter++;
      request(url).pipe(fs.createWriteStream(`./images/input_${fileName}.jpg`)).on('close', function () {
        cb(fileName, oldFile);
      });
    });
  } else {
    //ERROR TWEET
  }
}
